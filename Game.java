package com.valeness;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

class Game extends JPanel implements ActionListener, MouseListener {

    private Character character = new Character();

    private ArrayList<Entity> stack = new ArrayList<Entity>();
    private Screen screen;

    Game() {

    }

    public void run() {
        addKeyListener(new TAdapter(this.stack));
        addMouseListener(this);
        setFocusable(true);

        Timer timer = new Timer(8, this);
        timer.start();
    }

    public void addEntity(Entity entity) {
        stack.add(entity);
    }

    public void addScreen(Screen screen) {
        this.screen = screen;
    }

    private void drawBackground(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.drawBackground(g);

        int width = this.getWidth();
        int height = this.getHeight();

        for (Entity entity: stack) {
            entity.window_height = height;
            entity.window_width = width;

            entity.draw(g);
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (Entity entity: stack) {
            entity.handle();
        }
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        for (Entity entity: stack) {
            int x2 = entity.x + entity.width;
            int y2 = entity.y + entity.height;

            int click_x = e.getX();
            int click_y = e.getY();

            if(click_x <= x2 && click_x >= entity.x && click_y <= y2 && click_y >= entity.y) {
                entity.clickedMe(e);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
