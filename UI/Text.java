package com.valeness.UI;

import java.awt.*;
import java.awt.event.MouseEvent;

public class Text extends UI {
    public Text(String text, int x, int y) {
        super(x, y, 0, 0);
        this.text = text;
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        int width = g.getFontMetrics().stringWidth(this.text);
        this.setWidth(width);
    }

}
