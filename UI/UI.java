package com.valeness.UI;

import com.valeness.Entity;

import java.awt.*;

public class UI extends Entity {

    public UI(int x, int y, int width, int height) {
        super();

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.color = Color.RED;
    }

//    @Override
//    public void draw(Graphics g) {
//        g.setColor(this.color);
//        g.fillRect(this.x, this.y, this.width, this.height);
//    }
}