package com.valeness;

import com.valeness.UI.Text;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

public abstract class Entity implements InputHandler {

    protected int x = 0;
    protected int y = 0;
    protected int width = 0;
    protected int height = 0;

    public int window_width = 1;
    public int window_height = 1;

    int move_x = 0;
    int move_y = 0;

    private GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

    protected int img_width = 500;
    protected int img_height = 500;

    protected BufferedImage img;
    protected String text;

    protected Color color = Color.BLUE;

    public Entity loadImage(String sprite) {
        return this.loadImage(sprite, 1);
    }

    public Entity loadImage(String sprite, float scale) {
        try {
            this.img = ImageIO.read(getClass().getResourceAsStream(sprite));

            this.img_width = (int) (this.img.getWidth() * scale);
            this.img_height = (int) (this.img.getHeight() * scale);

            this.width = this.img_width;
            this.height = this.img_height;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return this;
    }

    public Entity setX(int x) {
        this.x = x;
        return this;
    }

    public Entity setY(int y) {
        this.y = y;
        return this;
    }

    public Entity setWidth(int width) {
        this.width = width;
        return this;
    }

    public Entity setHeight(int height) {
        this.height = height;
        return this;
    }

    @Override
    public void keyPressed(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {}

    @Override
    public void clicked(MouseEvent e) {}

    public void handle() {}

    public void clickedMe(MouseEvent e) {
        System.out.println("Clicked Me");
    }

    public void draw(Graphics g) {

        // Define the grid dimensions
        int grid_width = 100;
        int grid_height = 100;

        // Get screen dimensions
//        int screen_width = this.gd.getDisplayMode().getWidth() / 2;
//        int screen_height = this.gd.getDisplayMode().getHeight() / 2;

        // Calculate the translation from grid coords to screen pixels
        int x_percent = Math.round((float) this.x / grid_width * this.window_width);
        int y_percent = Math.round((float) this.y / grid_height * this.window_height);

        // Define the origin point of whatever we are drawing as the center
        int x_width_percent = Math.round((float) this.width / 2);
        int y_width_percent = Math.round((float) this.height / 2);

        // We draw based on top left corner coords, so change the top left coords to be offset for center origin
        x_percent = x_percent - x_width_percent;
        y_percent = y_percent - y_width_percent;

        // Set the color and draw the rectangle. Really only used for Debug/Simple UI
        g.setColor(this.color);
        g.fillRect(x_percent, y_percent, this.width, this.height);

        // If we have an image, draw it
        if(this.img != null){
            g.drawImage(this.img, x_percent, y_percent, this.img_width, this.img_height, null);
        }

        if(this instanceof Text && this.text != null) {
            g.drawString(this.text, x_percent, y_percent);
        }
    }

}