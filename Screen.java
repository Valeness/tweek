package com.valeness;

import javax.swing.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;

@SuppressWarnings("FieldCanBeLocal")
class Screen extends JFrame implements ComponentListener {

    private int window_x = 1080;
    private int window_y = 720;

    Screen() {

    }

    public void run() {
        initUI();
    }

    public void addGame(JPanel game) {
        add(game);
    }

    private void initUI() {

        setTitle("Adventure Quest!");
        setSize(this.window_x, this.window_y);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setVisible(true);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        int height = this.getHeight();
        int width = this.getWidth();
        System.out.println("Size: " + height + "x" + width);
    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }
}
