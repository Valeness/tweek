package com.valeness;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

interface InputHandler  {

    public void keyPressed(KeyEvent e);

    public void keyReleased(KeyEvent e);

    public void clicked(MouseEvent e);
}
