package com.valeness;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

class TAdapter extends KeyAdapter {

    private ArrayList<Entity> stack;

    TAdapter(ArrayList<Entity> stack) {
        super();
        this.stack = stack;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        for (Entity entity: this.stack) {
            entity.keyReleased(e);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        for (Entity entity: this.stack) {
            entity.keyPressed(e);
        }
    }
}