package com.valeness;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

class Character extends Entity {

    int health = 100;

    Character() {
        super();
        this.x = 50;
        this.y = 50;

        this.width = 68;
        this.height = 104;

        this.color = Color.GREEN;
    }

    public void handle() {
        this.x += this.move_x;
        this.y += this.move_y;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key_code = e.getKeyCode();
        switch (key_code) {
            case 38:
                this.health = this.health - 1;
                this.move_y = -1;
                break;

            case 40:
                this.move_y = 1;
                break;

            case 37:
                this.move_x = -1;
                break;

            case 39:
                this.move_x = 1;
                break;
        }


    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key_code = e.getKeyCode();
        switch (key_code) {
            case 40:
            case 38:
                this.move_y = 0;
                break;

            case 37:
            case 39:
                this.move_x = 0;
                break;

        }
    }

    @Override
    public void clickedMe(MouseEvent e) {
        System.out.println("Clicked Character");
    }
}
