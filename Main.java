package com.valeness;

import com.valeness.UI.*;
import com.valeness.UI.Menu;

import java.awt.*;

public class Main {
    public static void main(String[] args) {

        Screen screen = new Screen();

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                Game game = new Game();
                screen.addGame(game);
                screen.run();
                game.addScreen(screen);

                Character knight = (Character) new Character().loadImage("resources/knight.png", (float) 0.75).setX(25).setY(50);

                UI stats = new UI(50, 10, 300, 30);

                Text health = (Text) new Text(Integer.toString(knight.health), 50, 50);

                game.addEntity(stats);
                game.addEntity(knight);
                game.addEntity(health);

                game.run();
            }
        });
    }
}